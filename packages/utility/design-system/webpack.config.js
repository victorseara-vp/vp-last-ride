const { merge } = require('webpack-merge');
const singleSpaDefaults = require('webpack-config-single-spa-ts');
const path = require('path');

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: 'vp',
    projectName: 'design-system',
    webpackConfigEnv,
    argv,
  });

  const externals = webpackConfigEnv.standalone ? [''] : ['react', 'react-dom', 'react-router-dom', /^@vp\/.+/];

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object

    module: {
      rules: [
        {
          test: /\.(ts|tsx)$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
          },
        },
      ],
    },

    externals,
  });
};
