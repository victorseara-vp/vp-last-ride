import MaterialProvider from "../src/theme";

const withTheme = (Story, context) => (
  <MaterialProvider>
    <Story {...context} />
  </MaterialProvider>
);

export const decorators = [withTheme];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
};
