import { create } from "@storybook/theming/create";
import brand from "./brand.png";

export default create({
  brandTitle: "Vamos Parcelar",
  brandImage: brand,
});
