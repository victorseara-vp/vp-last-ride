// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import { Button, ButtonProps } from '@material-ui/core';

export default {
  title: 'Example/Button',
  component: Button,
  argTypes: {
    variant: {
      name: 'variant',
      defaultValue: 'contained',
      control: { type: 'select', options: ['contained', 'outlined', 'text'] },
    },
    color: {
      name: 'color',
      defaultValue: 'primary',
      control: { type: 'select', options: ['default', 'inherit', 'primary', 'secondary'] },
    },
  },
} as Meta;

const Template: Story<ButtonProps> = args => <Button {...args}>BUTTON</Button>;

export const Primary = Template.bind({});
Primary.args = {
  variant: 'contained',
};

export const Secondary = Template.bind({});
Secondary.args = {
  label: 'Button',
};
