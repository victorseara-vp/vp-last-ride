import theme from './theme';

export * from '@material-ui/core';

export { theme };
