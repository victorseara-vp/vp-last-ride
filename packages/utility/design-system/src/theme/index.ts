import { createMuiTheme, responsiveFontSizes } from '@material-ui/core';

const baseTheme = createMuiTheme({
  typography: {
    fontFamily: 'Montserrat',
  },
  palette: {
    primary: {
      main: '#4106CE',
    },
    secondary: {
      main: '#00E0D5',
    },
  },
});

const theme = responsiveFontSizes(baseTheme);

export default theme;
