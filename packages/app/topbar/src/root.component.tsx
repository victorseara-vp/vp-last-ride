import {
  ThemeProvider,
  AppBar,
  Toolbar,
  IconButton,
  theme,
  CssBaseline,
} from "@vp/design-system";
import { ExitToApp, Menu } from "@material-ui/icons";
import brand from "./assets/brand.png";

export default function Root() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar style={{ maxHeight: "64px" }} position="static">
        <Toolbar>
          <IconButton color="inherit" aria-label="open drawer" edge="start">
            <Menu />
          </IconButton>

          <img
            src={brand}
            height="36px"
            alt="brand"
            style={{ objectFit: "contain" }}
          />

          <IconButton
            style={{ position: "absolute", right: "1rem" }}
            color="inherit"
          >
            <ExitToApp />
          </IconButton>
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  );
}
